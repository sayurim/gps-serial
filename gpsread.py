import serial
import string
import math
import msvcrt

# Hora e Data
import datetime
from time import time
from time import sleep

# Check your COM port and baud rate
ser = serial.Serial(port='COM3',baudrate=115200, timeout=1)

f = open("GPS"+str(time())+".txt", 'w')

sleep(3)

while True:
	line = ser.readline()
	current_time = datetime.datetime.now().time()
	print(current_time,' ' ,line)                       # Write on console
	linetime = '{:%H:%M:%S:%f}'.format(current_time) + '\n' + line.decode('ascii')
	f.write(linetime)                                   # Write to the output log file
	if msvcrt.kbhit():
		key = msvcrt.getch()
		if key == 'X':                                  # escape key
			print ('Finishing...')
			break

ser.close
f.close
