import serial
import string
import math
import cv2

# Hora e Data
import datetime
from time import time
from time import sleep

#dmesg |grep tty
ser = serial.Serial('/dev/ttyACM0',baudrate=115200, timeout=1)

raw_data = open("GPS"+str(time())+".nmea", 'w')
timeline = open("GPS.txt", 'w')


while True:
	line = ser.readline()
	current_time = datetime.datetime.now().time()
	print(current_time,' ' ,line)                       # Write on console
	linetime = '{:%H:%M:%S:%f}'.format(current_time) + ',' + line.decode('ascii')
	timeline.write(linetime+'\n')
	raw_data.write(line)                      # Write to the output log file

	# frame=cv2.imread('HereGPS.jpg')
	# cv2.imshow('Input', frame)
	# c = cv2.waitKey(1)
	# if c == 27:
	# 	print ('Finishing...')
	# 	break

ser.close
f.close
