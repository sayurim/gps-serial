import serial
import string
import math
import cv2
#import msvcrt

# Hora e Data
import datetime
from time import time
from time import sleep

# Select Serial Port
serport = ''
serport = input("Serial Port? ")
# Check your COM port and baud rate
ser = serial.Serial(port=serport,baudrate=115200, timeout=1)
    
# f = open("GPS"+str(time())+".txt", 'w')
f = open("GPS"+".txt", 'w')

sleep(3)

while True:
	line = ser.readline()
	current_time = datetime.datetime.now().time()
	print(current_time,' ',line)                       # Write on console
	#linetime = '{:%H:%M:%S:%f}'.format(current_time) + ',' + line.decode('ascii')
	linetime = line.decode('ascii')                     # NMEA PURO
	f.write(linetime)                                   # Write to the output log file

	frame=cv2.imread('HereGPS.jpg')
	cv2.imshow('Input', frame)
	c = cv2.waitKey(1)
	if c == 27:
		print ('Finishing...')
		break

ser.close
f.close

